﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HCUSampleReminder.Business
{
    public class cHCUSampleReminder
    {
        public int iHCUSampleID { get; set; }
        public string MSCVesselName { get; set; }
        public string VesselEmailId { get; set; }
        public DateTime LastSampleReceivedDate { get; set; }
        public int NoOfSamplesReceived { get; set; }
    }
}
