﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Data.SqlClient;
using HCUSampleReminder.Business;
using System.Data;
using Microsoft.Extensions.Configuration;
using System;

namespace HCUSampleReminder
{
    partial class HCUService : ServiceBase
    {
        private EventLog _eventLog;
        private readonly Timer _timer;
        private readonly IConfiguration _configuration;
        public HCUService(IConfiguration configuration)
        {
            _configuration = configuration;
            _eventLog = new EventLog();

            if (!EventLog.SourceExists("HCUReminderLog"))
            {
                EventLog.CreateEventSource("HCUReminderLog", "HCUReminderLog");
            }

            _eventLog.Source = "HCUReminderLog";
            _eventLog.Log = "HCUReminderLog";

            double alertFrequency = 86400000;//86400000 = 24h
            if (_configuration["HCUReminder:AlertCheckFrequencyInMiliSeconds"] != string.Empty)
            {
                if (double.TryParse(_configuration["HCUReminder:AlertCheckFrequencyInMiliSeconds"], out var parsedNumber))
                {
                    alertFrequency = parsedNumber;
                }
            }
            _timer = new Timer(alertFrequency);
            _timer.Elapsed += _timerElapsedGeoFenceAlerts;
        }
        private void _timerElapsedGeoFenceAlerts(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            string day = DateTime.Now.DayOfWeek.ToString();
            if (day == "Friday")
                SendEmail();

            _timer.Start();
        }

        protected override void OnStart(string[] args)
        {
            _timer.Start();
            _eventLog.WriteEntry("HCU Sample Reminder: Service started", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            _timer.Stop();
            _eventLog.WriteEntry("HCU Sample Reminder: Service ended.", EventLogEntryType.Information);
        }
        protected override void OnPause()
        {
            _timer.Stop();
            _eventLog.WriteEntry("HCU Sample Reminder: Service paused.", EventLogEntryType.Information);
        }

        protected override void OnContinue()
        {
            _timer.Start();
            _eventLog.WriteEntry("HCU Sample Reminder: Service resumed.", EventLogEntryType.Information);
        }

        public void SendEmail()
        {
            string server = _configuration["HCUReminder:Server"];
            string database = _configuration["HCUReminder:Database"];
            string user = _configuration["HCUReminder:User"];
            string pwd = _configuration["HCUReminder:Pwd"];

            string sConnString = "server=" + server + ";database=" + database + ";User ID=" + user + ";Password=" + pwd + "; connect timeout=100000;";

            SqlConnection conn = new SqlConnection(sConnString);

            try
            {
                string sSql = ("SELECT VLIMS_HCUSampleID, VLIMS_MSCVesselName, VLIMS_VesselEmailId, VLIMS_LastSampleReceivedDate, VLIMS_NoOfSamplesReceived FROM VLIMS_HCUSampleReminder ");
                sSql += (" where (VLIMS_LastSampleReceivedDate <= DATEADD(day, -60, getdate()) or VLIMS_LastSampleReceivedDate = '1900-01-01') ");

                if (conn.State == ConnectionState.Closed)
                {
                    _eventLog.WriteEntry("connection opening " + conn + sConnString, EventLogEntryType.Information);
                    conn.Open();
                }

                _eventLog.WriteEntry("connection opened", EventLogEntryType.Information);
                SqlDataAdapter da = new SqlDataAdapter(sSql, conn);
                _eventLog.WriteEntry("HCU: cmd created", EventLogEntryType.Information);
                _eventLog.WriteEntry("HCU Service: SqlDataAdaptor created", EventLogEntryType.Information);
                DataTable dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var smtpClient = new SmtpClient
                    {
                        Host = "smtp.office365.com",
                        Port = 587,
                        UseDefaultCredentials = false,
                        EnableSsl = true,
                        Credentials = new System.Net.NetworkCredential("veems_email@theviswagroup.com", "Mus13235"),
                    };
                    var subject = "HCU Reminder Email";
                    string emailBody = string.Empty;

                    emailBody = "Dear Master/CE of " + dt.Rows[i]["VLIMS_MSCVesselName"].ToString() + "," + "<br/>" + "The HCU Sample test for the current quarter ending "
                        + Convert.ToDateTime(dt.Rows[i]["VLIMS_LastSampleReceivedDate"].ToString()).ToString("dd-MMM-yyyy") + " is due. You must have received the sample kits to send the sample to the lab. "
                        + "If you require kits or have any questions on the sampling procedure, please do not hesitate to contact us." +
                        "<br /><br />" + "This is an automated email generated by the system and if you have already dispatched the samples by the time, please ignore this email." + "<br /><br />" +
                        "Regards," + "<br />" + "The Viswa Lab Team";

                    var mailMessage = new MailMessage
                    {
                        From = new MailAddress("veems_email@theviswagroup.com"),
                        Subject = subject,
                        Body = emailBody,
                        IsBodyHtml = true
                    };
                    mailMessage.To.Add(dt.Rows[i]["VLIMS_VesselEmailId"].ToString());

                    smtpClient.Send(mailMessage);
                }
            }
            catch (Exception exception)
            {
                _eventLog.WriteEntry(exception.Message, EventLogEntryType.Warning);
            }
            finally
            {
                conn.Close();
            }

        }
    }
}
